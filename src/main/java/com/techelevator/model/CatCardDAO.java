package com.techelevator.model;

import java.util.List;

public interface CatCardDAO {
	public List<CatCard> getAllCards();
	public CatCard getOneCard(long id);
	public CatCard saveCard(CatCard cardToSave);
	public CatCard updateCard(CatCard changedCard);
	public boolean removeCard(long id);
}
