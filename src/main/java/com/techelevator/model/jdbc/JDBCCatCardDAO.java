package com.techelevator.model.jdbc;

import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.model.CatCard;
import com.techelevator.model.CatCardDAO;

@Component
public class JDBCCatCardDAO implements CatCardDAO {

	private JdbcTemplate sql; 
	
	@Autowired
	public JDBCCatCardDAO(BasicDataSource dataSource) {
		this.sql = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<CatCard> getAllCards() {
		String query = "SELECT id, img_url, fact, caption FROM catcards ";
		return sql.query(query, cardMapper);
	}
	
	@Override
	public CatCard getOneCard(long id) {
		String query = "SELECT id, img_url, fact, caption FROM catcards WHERE id = ? ";
		return sql.queryForObject(query, cardMapper, id);
	}

	@Override
	public CatCard updateCard(CatCard changedCard) {
		String query = "UPDATE catcards SET img_url = ?, fact = ?, caption = ? WHERE id = ? ";
		sql.update(query, changedCard.getImgUrl(), changedCard.getCatFact(), changedCard.getCaption(), changedCard.getCatCardId());
		return changedCard;
	}

	@Override
	public boolean removeCard(long id) {
		String query = "DELETE FROM catcards WHERE id = ? ";
		return sql.update(query, id) == 1;
	}

	@Override
	public CatCard saveCard(CatCard cardToSave) {
		String query = "INSERT INTO catcards (id, img_url, fact, caption) VALUES (DEFAULT, ?, ?, ?) RETURNING id ";
		SqlRowSet result = sql.queryForRowSet(query, cardToSave.getImgUrl(), cardToSave.getCatFact(), cardToSave.getCaption());
		Long id = result.next() ? result.getLong(1) : null;
		cardToSave.setCatCardId(id);
		return cardToSave;
	}
	
	private RowMapper<CatCard> cardMapper = (row, rowNum) -> {
		CatCard cc = new CatCard();

		cc.setCatCardId(row.getLong("id"));
		cc.setCatFact(row.getString("fact"));
		cc.setImgUrl(row.getString("img_url"));
		cc.setCaption(row.getString("caption"));
		
		return cc;
	};
}
