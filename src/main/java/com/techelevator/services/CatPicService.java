package com.techelevator.services;

import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.techelevator.model.CatPic;

public class CatPicService {

	private static final String API_URL = "https://aws.random.cat/meow";
	private RestTemplate restTemplate = new RestTemplate();
	
    public CatPic getPic() throws RestClientResponseException {
    	CatPic catPic = restTemplate.getForObject(API_URL, CatPic.class);
        return catPic;
    }

}
