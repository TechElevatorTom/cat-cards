package com.techelevator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.techelevator.model.CatCard;
import com.techelevator.model.CatCardDAO;
import com.techelevator.model.CatFact;
import com.techelevator.model.CatPic;
import com.techelevator.services.CatFactService;
import com.techelevator.services.CatPicService;

@RestController
@RequestMapping("/api/card")
public class CatController {

	@Autowired
	private CatCardDAO cat;
	
	private CatFactService catFact = new CatFactService();
	private CatPicService catPic = new CatPicService();
	
	@GetMapping
	public CatCard makeNewCard() {
		CatFact f = catFact.getFact();
		CatPic p = catPic.getPic();

		CatCard c = new CatCard();
		c.setCatFact(f.getText());
		c.setImgUrl(p.getFile());
		
		return c;
	}
	
	@GetMapping("/{id}")
	public CatCard getIndividualCard(@PathVariable long id) {
		return cat.getOneCard(id);
	}
	
	@GetMapping("/all")
	public List<CatCard> getAllCards() {
		return cat.getAllCards();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public CatCard saveNewCard(@RequestBody CatCard incomingCard) {
		return cat.saveCard(incomingCard);
	}
	
	@PutMapping("/{id}")
	public CatCard updateExistingCard(@PathVariable long id, @RequestBody CatCard changedCard) {
		if(cat.getOneCard(id) != null) {
			return cat.updateCard(changedCard);
		}
		return null;
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteExistingCard(@PathVariable long id) {
		if(cat.getOneCard(id) != null) {
			cat.removeCard(id);
		}
	}
}
