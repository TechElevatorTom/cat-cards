# Cat Cards!

### Context
Congratulations on your new job at _HazBro_, where you have been placed on a project to develop their latest project, *Cat Cards*. To facilitate development, the VP in charge of this project has split the development staff into three teams: Database, Front End and Back End. You have been placed on the Back End team.  

The Front End team has developed their part of the application. However, they need you to provide the data to get it to work properly. They have given you the following API documentation:


`GET /api/card` : Provides a new, randomly created Cat Card  
`GET /api/card/{id}` : Provides a Cat Card with the given id  
`GET /api/card/all` : Provides a list of all Cat Cards in the user's collection  
`POST /api/card` : Saves a card to the user's collection  
`PUT /api/card` : Updates a card in the user's collection  
`DELETE /api/card` : Removes a card from the user's collection  
  
**Cat Card JSON object structure:**
```
{
    "id" : an integer that represents this particular card's unique identifier,
    "imgUrl" : "A string containing the full URL to the cat image",
    "fact" : "A string containing a cat fact",
    "caption" : "A string containing a the caption for this particular card"
}
```

**Cat Card collection example:**
```
[

    {
        "id" : 17,
        "imgUrl" : "https://purr.objects-us-east-1.dream.io/i/8M3AW.jpg",
        "fact" : "Cats sleep 70% of their lives.",
        "caption" : "Aww, this reminds me of Lefty! He slept CONSTANTLY."
    },

    {
        "id" : 38,
        "imgUrl" : "https://purr.objects-us-east-1.dream.io/i/image.jpeg",
        "fact" : "People who own cats have on average 2.1 pets per household, where dog owners have about 1.6.",
        "caption" : "Bartender, I'll take a Salty *Cat*"
    }
]
```

The Database team has provided you with a script to create a database locally, and also the DAO files to retrieve the data. You should be able to implement your code without needing to review the implementation details of the DAO files -- the interface will provide you with enough information to complete your tasks.


Once you provide implementation for the above endpoints, returning properly formatted JSON objects in the agreed-upon schema, the application will work!


### Getting Started
1. Install your database by running `database/create_db.sh`  
1. Launch this project and navigate to `http://localhost:8080/CatCards/`  
